import os
from background_task import background
from .models import Client, Broadcast, Message
import datetime
import asyncio
import aiohttp
import logging
from django.utils import timezone
from channels.db import database_sync_to_async
from aiohttp import ClientRequest, ClientSession
import logging
import pytz
from yarl import URL

@database_sync_to_async
def update_message_status(message_id, status):
    message = Message.objects.get(id=message_id)
    message.status = status
    message.sent_time = timezone.now()
    message.save()

@database_sync_to_async
def schedule_resend_message(payload,end_time,time):
    resend_message(payload, end_time.isoformat(), schedule=time)

async def send_message(payload,end_time):
    url = f'https://probe.fbrq.cloud/v1/send/{payload["id"]}'
    #url = URL(url)
    headers = {"Authorization": f"Bearer {os.environ['user_pool']}"}
    response = None
    if  timezone.now() > end_time:
        response='late'
        update_message_status(payload["id"], response)
        return
    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(url, json=payload, headers=headers) as response:
            #request = aiohttp.ClientRequest(method='POST', url=url, data=payload, headers=headers)
            #response = await session.send(request)
                await  response.text()
                response = response.status
    except aiohttp.ClientError as e:
        response = str(e)
    except asyncio.TimeoutError:
        time = timezone.now() + datetime.timedelta(minutes=5)
        await schedule_resend_message(payload, end_time, time)
        response = 'timeout'
    finally:
        await update_message_status(payload["id"], response)

@database_sync_to_async
def create_message(broadcast_id,client_id):
    message = Message(status='started', broadcast_id=broadcast_id, client_id=client_id,
                      sent_time=timezone.now())
    message.save()
    return(message.id)

async def set_up_messages(broadcast_id,broadcast_text, clients, end_time):
    tasks = []
    for client in clients:
        message_id = await create_message(broadcast_id,client.id)
        payload = {
            "id": message_id,
            "phone": client.id,
            "text": broadcast_text
        }
        task = asyncio.ensure_future(send_message(payload, end_time))
        tasks.append(task)
    await asyncio.gather(*tasks)


@background()
def start_broadcast(broadcast_id):

    broadcast = Broadcast.objects.get(id=broadcast_id)
    if (timezone.now() >= broadcast.end_time):
        return
    clients = list(Client.objects.filter(
        operator_code=broadcast.operator_code,
        tags=broadcast.tags
    ))
    loop = asyncio.new_event_loop()
    loop.run_until_complete(set_up_messages(broadcast.id, broadcast.text, clients,broadcast.end_time))
    loop.close()


@background()
def resend_message(payload,end_time):
    end_time = datetime.datetime.fromisoformat(end_time)
    loop = asyncio.new_event_loop()
    loop.run_until_complete(send_message(payload, end_time))
    loop.close()