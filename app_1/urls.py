from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ClientViewSet, BroadcastViewSet

router = DefaultRouter()
router.register(r'clients', ClientViewSet, basename='client')
router.register(r'broadcasts', BroadcastViewSet, basename='broadcast')

urlpatterns = [
    path('', include(router.urls)),
]
