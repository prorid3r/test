from django.db import models
from phonenumber_field.formfields import PhoneNumberField
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.postgres.fields import ArrayField
from timezone_field import TimeZoneField
# Create your models here.


class Broadcast(models.Model):
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    text = models.CharField(max_length=300)
    operator_code = models.IntegerField(
        validators=[
            MinValueValidator(100, message='Minimum 3 digits required.'),
            MaxValueValidator(999, message='Maximum 3 digits exceeded.')
        ]
    )
    tags = ArrayField(
        models.CharField(max_length=10),
        blank=True,
        null=True,
        size=10,
    )


class Client(models.Model):
    number = PhoneNumberField(region="RU")
    operator_code = models.IntegerField(
        validators=[
            MinValueValidator(100, message='Minimum 3 digits required.'),
            MaxValueValidator(999, message='Maximum 3 digits exceeded.')
        ]
    )
    tags = ArrayField(
        models.CharField(max_length=10),
        blank=True,
        null=True,
        size=10,
    )
    timezone = TimeZoneField(use_pytz=True)

class Message(models.Model):
    #STATUS_CHOICES = (
    #    ('sent', 'Sent'),
    #    ('failed', 'Failed'),
    #    ('started', 'Started'),
    #)  
    sent_time = models.DateTimeField()
    status = models.CharField(max_length=20)
    broadcast = models.ForeignKey(Broadcast, on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE)



