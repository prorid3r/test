from datetime import datetime

from drf_spectacular.utils import extend_schema

from .serializers import ClientSerializer, BroadcastCreateSerializer, BroadcastRetrieveSerializer, BroadcastsStatsSerializer
from .models import Client, Broadcast
from rest_framework import viewsets, status
from rest_framework.response import Response
from .tasks import start_broadcast

class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

class BroadcastViewSet(viewsets.ModelViewSet):
    queryset = Broadcast.objects.all()
    serializer_class = BroadcastCreateSerializer

    @extend_schema(summary="Добавить новую рассылку. Создает новую задачу на рассылку сообщений, начинающуюся в 'start_date'")
    def create(self, request, *args, **kwargs):
        serializer = BroadcastCreateSerializer(data = request.data)
        if serializer.is_valid():
            res = serializer.save()
            start_date = serializer.data['start_time']
            start_broadcast(res.id,schedule=datetime.strptime(start_date, "%Y-%m-%dT%H:%M:%S.%fZ"))
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema( responses={status.HTTP_200_OK: BroadcastRetrieveSerializer,})
    def retrieve(self, request, *args, **kwargs):
        broadcast = self.get_object()  # Get the object based on the provided ID
        broadcast = Broadcast.objects.prefetch_related('messages').get(id=broadcast.pk)
        serializer = BroadcastRetrieveSerializer(broadcast)
        return Response(serializer.data)

    @extend_schema(responses={status.HTTP_200_OK: BroadcastsStatsSerializer, }, summary="Статистика по всем рассылкам")
    def list(self, request, *args, **kwargs):
        #
        queryset = Broadcast.objects.prefetch_related('messages')
        serializer = BroadcastRetrieveSerializer(queryset, many=True)
        message_data={}
        broadcast_data = serializer.data
        for broadcast in broadcast_data:
            broadcast_id = broadcast['id']
            message_data[broadcast_id] = {}
            for message in broadcast['messages']:
                status = message['status']
                message_data[broadcast_id].setdefault(status, 0)
                message_data[broadcast_id][status] += 1
            broadcast['messages'] =  [{'status': status, 'count': count} for status, count in message_data[broadcast_id].items()]
        serializer = BroadcastsStatsSerializer(data=broadcast_data,many=True)
        if serializer.is_valid():
            return Response(broadcast_data)