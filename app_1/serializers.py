from rest_framework import serializers
from .models import Client, Broadcast, Message
from timezone_field.rest_framework import TimeZoneSerializerField

class ClientSerializer(serializers.ModelSerializer):
    timezone = TimeZoneSerializerField(use_pytz=True)
    class Meta:
        model = Client
        fields = '__all__'

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'

class BroadcastCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Broadcast
        fields = '__all__'

class BroadcastRetrieveSerializer(serializers.ModelSerializer):
    messages = MessageSerializer(many=True)
    class Meta:
        model = Broadcast
        fields = '__all__'

class MessageStatisticsSerializer(serializers.Serializer):
    status = serializers.CharField()
    count = serializers.IntegerField()

class BroadcastsStatsSerializer(serializers.ModelSerializer):
    messages = MessageStatisticsSerializer(many=True)
    class Meta:
        model = Broadcast
        fields = '__all__'